#include "functional.h"

auto is_not_female(const person_t &person) -> bool { return !person.is_female; }
auto is_female(const person_t &person) -> bool { return person.is_female; }
auto name(const person_t &person) -> string { return person.name; }