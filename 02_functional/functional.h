//
// Created by practice on 28.11.19.
//
#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <algorithm>
/*
 * count, transform
 */

#include <numeric>
/*
 * accumulate
 */

using std::vector, std::string;

auto count_lines_in_files(const vector<string> &files) -> vector<int>;

auto average_score(const vector<int> &scores) -> double;

auto scores_product(const vector<int> &scores) -> double;

auto trim(string s) -> string;

struct person_t {
  string name;
  bool is_female;
};

auto is_not_female(const person_t &person) -> bool;
auto is_female(const person_t &person) -> bool;
auto name(const person_t &person) -> string;

auto get_names(const vector<person_t> &people) -> vector<string>;

auto get_names_simpler(const vector<person_t> &people) -> vector<string>;

template <typename FilterFunction>
auto names_for(const vector<person_t>& people, FilterFunction filter) -> vector<string>;