# modernCpp

miscellaneous  little projects.
  
#### 01_arcanoid_tutorial:  
brace initialization list | emplace_back | for each loop | auto |  
template function | function overloading | ternary operators |  
erase-remove idiom | constexpr | SFML init | basic collisions |  

#### 02_functional

`f: (args) -> output`  
  
std::count  
std::transform  
std::accumulate  
std::find_if  
std::partition  
std::stable_partition  
