//
// Created by practice on 28.11.19.
//

#include "functional.h"

using std::count, std::transform;

using std::ifstream;
using std::istreambuf_iterator;

auto count_lines(const string &filename) -> int {
    ifstream in(filename);

    return count(istreambuf_iterator<char>(in),  // Counts newlines from the current position
                 istreambuf_iterator<char>(),    // in the stream until the end of the file
                 '\n');
}

auto count_lines_in_files(const vector<string> &files) -> vector<int> {
    vector<int> results(files.size());

    transform(files.cbegin(), files.cend(), // <- Specifies which items to transform | T 
              results.begin(),              // <- Where to store the results         | container<R>
              count_lines);                 // <- Transformation function            | f: (T) -> R, R +> container<R>
                                            //                                       | container<T> -> container<R>
    return results;
}

// implementation with std::accumulate

auto f(int previous_count, char c) -> int {
    return (c != '\n') ? previous_count
                       : previous_count + 1;
}

auto count_lines1(const string &s) -> int { //                 | -> R
    return std::accumulate(
            s.cbegin(), s.cend(), // <- Folds entire string    | T
            0,                    // <- Start the count with 0 | R
            f                     // <- Applys the function    | f: (R, T) -> R
    );
}

/*
 * std::accumulate used left-associative binary operator such as +
 * and performs left-folding
 *
 * if we want to perform right-folding, we should pass to std::accumulate
 * reverse iterators: (crbegin and crend)
 */
