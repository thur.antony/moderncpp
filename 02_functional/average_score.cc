//
// Created by practice on 28.11.19.
//

#include "functional.h"

using std::accumulate;

auto average_score(const vector<int> &scores) -> double {
    return accumulate(scores.cbegin(), scores.cend(), 0) // sequentially sums all values
           /
           static_cast<double>(scores.size());
}

auto scores_product(const vector<int> &scores) -> double {
    return accumulate(scores.cbegin(), scores.cend(), 1, std::multiplies<int>());
} // std::accumulate is a higher order function because it accepts function as a parameter

// goto count_lines_in_files.cc :) to find further explanation of std::accumulate
