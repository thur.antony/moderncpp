//
// Created by practice on 28.11.19.
//

#include "functional.h"

using std::partition, std::stable_partition;

using std::copy_if, std::transform;

using std::back_inserter;

/*
 * collection<T> -> std::partition -> reordered collection<T>
 * collection<T> : [elements which satisfy predicate][which don't]
 *
 * std::partition: (collection<T>, predicate) -> iterator to the first elem which don't
 *
 * std::stable_partition retains the ordering between elements from the same group
 */

/*
 * erase-remove idiom : collection.erase(remove_if(begin, end, f), end); - OK if we are allowed to modify collection
 *
 * src_collection c;
 * copy_if(c.cbegin, c.cend, back_inserter(dest_collection), f);
 *
 * back_inserter(c) -- c is a container which supports a push_back operation
 */

auto get_names(const vector<person_t> &people) -> vector<string> {
    // pick females
    vector<person_t> females;
    copy_if(people.cbegin(), people.cend(),
            back_inserter(females),
            is_female);
    // extract their names
    vector<string> names(females.size());
    transform(females.cbegin(), females.cend(),
              names.begin(),
              name);
    return names;
}

auto get_names_simpler(const vector<person_t> &people) -> vector<string> {
    vector<string> names;

    for (const auto &person : people) {
        if (is_female(person))
            names.push_back(name(person));
    }

    return names;
}

template <typename FilterFunction>
auto names_for(const vector<person_t>& people, FilterFunction filter) -> vector<string> {
    vector<string> results;
    
    for (const person_t& person : people) {
        if (filter(person)) {
            results.push_back(name(person));
        }
    }
    
    return results;
}