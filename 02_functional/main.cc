//
// Created by practice on 28.11.19.
//
#include "functional.h"

using std::cout, std::endl;

const vector<string> files{
    "../02_functional/files/file1",
    "../02_functional/files/file2",
    "../02_functional/files/file3",
};

const vector<int> scores{
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
};

const string s{"    <-trimmed->   "};

const vector<person_t> people{
    {"Kate", true},
    {"Sasha", true},
    {"Bob", false},
    {"Sonia", false},
    {"Sue", true},
};

int main() {
    cout << "-> count_lines_in_files: std::count, std::transform" << endl;
    auto lines_counted{count_lines_in_files(files)};
    for (auto &lines : lines_counted)
        cout << lines << endl;

    cout << "-> average_score: std::accumulate" << endl;
    cout << average_score(scores) << endl;

    cout << "-> scores_product: std::accumulate + binary_op std::multiply" << endl;
    cout << scores_product(scores) << endl;

    cout << "-> trim: std::find_if" << endl;
    cout << "|" << trim(s) << "|" << endl;
    
    cout << "-> get_names: std::copy_if, std::transform" << endl;
    for (auto &name : get_names(people))
        cout << name << endl;

    cout << "-> get_names_simpler" << endl;
    for (auto &name : get_names_simpler(people))
        cout << name << endl;
    
    // cout << "-> own_filter" << endl;
    // for (auto &name : names_for(people, is_not_female))
    //     cout << name << endl;
 
    return 0;
}
