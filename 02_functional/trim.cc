//
// Created by practice on 28.11.19.
//

#include "functional.h"

using std::find_if; // searches for the first item in the collection
                    // that satisfies the specified predicate.
                    // it's a higher order function because we passed into it `is_not_space`

auto is_not_space(char &c) -> bool {
    return !isspace(c);
}

auto trim_left(string s) -> string {
    s.erase(s.begin(),
            find_if(s.begin(), s.end(), is_not_space));
    return s;
}

auto trim_right(string s) -> string {
    s.erase(find_if(s.rbegin(), s.rend(), is_not_space).base(), // .base() converts reverse iterator to forvard iterator
            s.end());
    return s;
}

auto trim(string s) -> string {
    return trim_left(trim_right(std::move(s)));
}
